output "region" {
  value       = var.region
  description = "GCloud Region"
}

output "project_id" {
  value       = var.project_id
  description = "GCloud Project ID"
}

output "network_name" {
  value       = google_compute_network.vpc.name
  description = "GKE VPC Name"
}

output "subnetwork_name" {
  value       = google_compute_subnetwork.subnet.name
  description = "GKE Subnet Name"
}