variable "project_id" {
  description = "project id"
}

variable "region" {
  description = "region"
  default     = "us-east1"
}