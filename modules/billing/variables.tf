variable "billing_account_id" {
  description = "GCloud Billing Account ID"
}

variable "budget_name" {
  description = "Name of budget"
  default     = "Project Budget"
}

variable "budget_currency" {
  description = "Budget currency"
  default     = "USD"
}

variable "budget_amount" {
  description = "Budget amount (units)"
  default     = "100"
}

variable "threshold_actual_percentage" {
  description = "Actual percentage threshold"
  default     = 1.0
}

variable "threshold_forecasted_percentage" {
  description = "Forecasted percentage threshold"
  default     = 0.5
}

variable "disable_default_iam_recipients" {
  description = "Disabled default billing recipients"
  default     = false
}

variable "notification_channel_name" {
  description = "Name of notification channel"
  default     = "Budget Notification Channel"
}

variable "notification_email_address" {
  description = "Email address to notify on budget"
}
