resource "google_billing_budget" "budget" {
  billing_account = data.google_billing_account.account.id
  display_name    = var.budget_name

  budget_filter {
    projects = ["projects/${data.google_project.project.number}"]
  }

  amount {
    specified_amount {
      currency_code = var.budget_currency
      units         = var.budget_amount
    }
  }

  threshold_rules {
    threshold_percent = var.threshold_actual_percentage
  }
  threshold_rules {
    threshold_percent = var.threshold_forecasted_percentage
    spend_basis       = "FORECASTED_SPEND"
  }

  all_updates_rule {
    monitoring_notification_channels = [
      google_monitoring_notification_channel.email_notification_channel.id,
    ]
    disable_default_iam_recipients = var.disable_default_iam_recipients
  }
}

resource "google_monitoring_notification_channel" "email_notification_channel" {
  display_name = var.notification_channel_name
  type         = "email"

  labels = {
    email_address = var.notification_email_address
  }
}