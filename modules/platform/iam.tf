resource "google_service_account" "sa" {
  account_id   = "platform-svc"
  display_name = "Platform Service Account"
}

resource "google_project_iam_member" "storage_admin" {
  project = data.google_project.project.number
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.sa.email}"
}

resource "google_project_iam_member" "cluster_viewer" {
  project = data.google_project.project.number
  role    = "roles/container.developer"
  member  = "serviceAccount:${google_service_account.sa.email}"
}