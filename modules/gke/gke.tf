# GKE cluster
resource "google_container_cluster" "primary" {
  name     = "gke-${var.project_id}"
  location = var.region

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  network    = var.gke_network_name
  subnetwork = var.gke_subnetwork_name
}

# Separately Managed Node Pool
resource "google_container_node_pool" "primary_nodes" {
  name       = google_container_cluster.primary.name
  location   = var.region
  cluster    = google_container_cluster.primary.name
  node_count = var.gke_num_nodes

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = var.project_id
    }

    spot         = true
    machine_type = "n1-standard-1"
    tags         = ["gke-node", "gke-${var.project_id}"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}
