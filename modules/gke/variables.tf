variable "project_id" {
  description = "GCloud Project ID"
}

variable "region" {
  description = "GCloud Region"
  default     = "us-east1"
}

variable "gke_network_name" {
  description = "GKE network name"
}

variable "gke_subnetwork_name" {
  description = "GKE network name"
}

variable "gke_num_nodes" {
  description = "Number of GKE nodes"
  default     = 1
}