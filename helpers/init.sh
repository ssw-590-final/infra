#!/bin/sh
set -euo pipefail

# OVERRIDE AS NEEDED
SA_CREDS_PATH=${SA_CREDS_PATH:-creds.json}

# GCLOUD UPDATE
echo "Updating gcloud components..."
gcloud components update

# GCP AUTH
gcloud auth list
read -p "[?] Switch GCP auth? [y/n] " SWITCH_AUTH
if [ $SWITCH_AUTH = "y" ]; then
  read -p 'GCP Account name: ' ACCOUNT_NAME
  gcloud config set account $ACCOUNT_NAME
fi

# GCP PROJECT
read -p "[?] Create a new GCP Project? [y/n] " CREATE_NEW
if [ $CREATE_NEW = "y" ]; then
  read -p 'PROJECT ID (e.g. my-demo-123456): ' PROJECT_ID
  echo "[*] Creating project $PROJECT_ID..."
  gcloud projects create $PROJECT_ID
  gcloud config set project $PROJECT_ID
else
  PROJECT_ID=$(gcloud config list project | grep 'project' | sed -ne 's/^project = //p')
  echo "[*] Using project: $PROJECT_ID"
fi

# GCP PROJECT APIs
read -p "[?] Enable APIs? [y/n] " ENABLE_APIS
if [ $ENABLE_APIS = "y" ]; then
  echo "[*] Enabling required APIs for $PROJECT_ID..."
  echo "[*] [ cloudbilling.googleapis.com ]"
  gcloud services enable cloudbilling.googleapis.com
  echo "[*] [ billingbudgets.googleapis.com ]"
  gcloud services enable billingbudgets.googleapis.com
  echo "[*] [ cloudresourcemanager.googleapis.com ]"
  gcloud services enable cloudresourcemanager.googleapis.com
  echo "[*] [ compute.googleapis.com ]"
  gcloud services enable compute.googleapis.com || (echo "[!] You FIRST must go to https://console.cloud.google.com/billing/linkedaccount?referrer=search&project=$PROJECT_ID to enable billing in this project!" && echo "[!] Then rerun gcloud services enable compute.googleapis.com" && exit 1)
  echo "[*] [ container.googleapis.com ]"
  gcloud services enable container.googleapis.com || (echo "[!] You FIRST must go to https://console.cloud.google.com/billing/linkedaccount?referrer=search&project=$PROJECT_ID to enable billing in this project!" && echo "[!] Then rerun gcloud services enable compute.googleapis.com" && exit 1)
fi

# GCS BUCKET
read -p "[?] Create a new GCS bucket? [y/n] " CREATE_NEW_BUCKET
if [ $CREATE_NEW_BUCKET = "y" ]; then
  read -p 'BUCKET NAME (e.g. my-demo-tfstate): ' TF_BUCKET_NAME
  echo "[*] Creating GCS bucket $TF_BUCKET_NAME..."
  gcloud storage buckets create gs://$TF_BUCKET_NAME
else
  echo "[*] Available GCS buckets in $PROJECT_ID..."
  gcloud storage buckets list --format="json(name)"
  read -p 'Please select GCS bucket to use (e.g. my-demo-tfstate): ' TF_BUCKET_NAME
fi

# GCP IAM SERVICE ACCOUNT
read -p "[?] Create new Service Account? [y/n] " CREATE_NEW_SA
if [ $CREATE_NEW_SA = "y" ]; then
  read -p 'SERVICE ACCOUNT NAME (e.g. infra-svc): ' SERVICE_ACCOUNT_NAME
  echo "[*] Creating Service Account named $SERVICE_ACCOUNT_NAME..."
  gcloud iam service-accounts create $SERVICE_ACCOUNT_NAME \
    --display-name="Terraform Service Account"
  echo "[*] Generating and downloading credentials for $SERVICE_ACCOUNT_NAME to $SA_CREDS_PATH..."
  gcloud iam service-accounts keys create $SA_CREDS_PATH \
    --iam-account=$SERVICE_ACCOUNT_NAME@$PROJECT_ID.iam.gserviceaccount.com
else
  echo "[*] Available Service Accounts in $PROJECT_ID..."
  gcloud iam service-accounts list
  read -p 'Please select Service Account to use (e.g. infra-svc): ' SERVICE_ACCOUNT_NAME
fi

# GCP IAM SERVICE ACCOUNT PERMISSIONS
read -p "[?] Create Service Account policies? [y/n] " BIND_SA
if [ $BIND_SA = "y" ]; then
  echo "[*] Granting service account $SERVICE_ACCOUNT_NAME access bucket $TF_BUCKET_NAME..."
  gcloud storage buckets add-iam-policy-binding gs://$TF_BUCKET_NAME \
    --member=serviceAccount:$SERVICE_ACCOUNT_NAME@$PROJECT_ID.iam.gserviceaccount.com \
    --role=roles/storage.objectCreator

  echo "[*] Granting service account $SERVICE_ACCOUNT_NAME roles/editor access in project $PROJECT_ID..."
  gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member "serviceAccount:$SERVICE_ACCOUNT_NAME@$PROJECT_ID.iam.gserviceaccount.com" \
    --role "roles/editor"

  # Allow SA cost manager access to GCP Billing account
  read -p "[*] Enter GCP Billing account ID (https://console.cloud.google.com/billing): " BILLING_PROJECT_ID
  gcloud beta billing accounts add-iam-policy-binding $BILLING_PROJECT_ID \
    --member "serviceAccount:$SERVICE_ACCOUNT_NAME@$PROJECT_ID.iam.gserviceaccount.com" \
    --role "roles/billing.costsManager"

fi

echo "[!] Be sure to export your Service Account credentials file to GOOGLE_APPLICATION_CREDENTIALS to use them with Terraform!"
echo "[!] For ex: 'export GOOGLE_APPLICATION_CREDENTIALS=\$(pwd)/creds.json'"