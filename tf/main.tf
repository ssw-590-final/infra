module "vpc" {
  source     = "../modules/vpc"
  project_id = var.project_id
  region     = var.region
}

module "gke" {
  source = "../modules/gke"

  project_id = var.project_id
  region     = var.region

  gke_network_name    = module.vpc.network_name
  gke_subnetwork_name = module.vpc.subnetwork_name

  gke_num_nodes = 1
}

module "billing" {
  source = "../modules/billing"

  billing_account_id         = var.billing_account_id
  notification_email_address = var.notification_email_address
}

module "platform" {
  source = "../modules/platform"
}