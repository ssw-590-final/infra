variable "region" {
  description = "region"
  default     = "us-east1"
}

## OVERRIDE THESE VARIABLES:
# variable "credentials_path" {
#   description = "path to GCP SA credentials"
# }
variable "project_id" {
  description = "project id"
}

variable "billing_account_id" {
  description = "billing account id"
}

variable "notification_email_address" {
  description = "email address to notify on budget"
}