# infra

[![pipeline status](https://gitlab.com/ssw-590-final/infra/badges/main/pipeline.svg)](https://gitlab.com/ssw-590-final/infra/-/commits/main)

Manages infrastructure for the ssw-590-final Lab using Terraform.

> [Check out the demo video here!](https://vimeo.com/819746040?share=copy)

[[_TOC_]]

## Getting Started

This repository bootstraps and manages the infrastructure for a GKE cluster in GCP. (As of Feb 2023) GCP is offering a [**free trail**](https://cloud.google.com/free/), including $300 in credits ($400 for students) for 90d when starting out on their platform.

## A. Prerequisites

### 1. Create a GitLab account

We will be using GitLab to manage and deploy our infrastructure via [CI Pipelines](https://docs.gitlab.com/ee/ci/). If you do not yet have an account, create one and follow the steps to configure access from your local machine.

### 2. Fork this repo

Once you have your GitLab account and local access configured, fork this repo and clone it locally so you can commit changes!

![Fork](img/fork.png){width=25%}

I recommend setting your project as `Private`, so you have full control over who has access to it:
![Private](img/fork-private.png){width=75%}

### 3. Create a GCP account

> **WARNING:** This free trial offer **does require a credit card** to recieve the credits. It is your responsibility to keep track of your GCP billing account spending, and tear down any infrastructure you are no longer using!

Go to [cloud.google.com/free](https://cloud.google.com/free/) to sign up for a GCP free trial. If you have a student email, I recommend using it as they may offer an additional $100 in credits. 

Follow the prompts to create your account.

### 4. Install tools

Install the follows tools locally:
* [git]((https://git-scm.com/book/en/v2/Getting-Started-Installing-Git))
* [gcloud](https://cloud.google.com/sdk/docs/install)
* [kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl)


## B. Configure GCP Requirements

You can follow the steps below, **or** run the [`./helpers/init.sh` script](./helpers/init.sh) to configure your GCP project requirements.

### 1. Create a GCP Project
You can create a new GCP project specifically for this demo, or use the default `My First Project`. 

**Keep reference to your `Project ID`, as it will be used often in the coming steps.**

```bash
export PROJECT_ID=my-demo-123456
gcloud projects create $PROJECT_ID
gcloud config set project $PROJECT_ID
```

### 2. Enable APIs

You need to enable a number of APIs in order to create resources in later steps.

```bash
gcloud services enable cloudbilling.googleapis.com
gcloud services enable billingbudgets.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
```

Before installing the remaining APIs, go to the GCP console > Search for `Linked Account` > `Select Project` > Your newly created project > `Link to Billing Account`.

![Linked Account](./img/linked-account.png){width=50%}

![Link to Billing Account](./img/link-to-billing.png){width=50%}

Then you can enable the final APIs:
```bash
gcloud services enable compute.googleapis.com
gcloud services enable container.googleapis.com
```

You can check which services are currently enable in your project by running

```bash
gcloud services list --enabled --project $PROJECT_ID
```

### 3. Create GCS Bucket for Terraform State

Terraform state will be managed in a GCS bucket. Create this bucket and store the bucket ID for later use.

> TIP: Be sure to give your bucket a globally unique name!

```bash
export TF_BUCKET_NAME=my-demo-tfstate
gcloud storage buckets create gs://$TF_BUCKET_NAME
```

### 4. Create GCP Service Account for Terraform

You will need a GCP Service Account and key to manage infrastructure via terraform. 

```bash
export SERVICE_ACCOUNT_NAME=infra-svc
export SA_CREDS_PATH=$(pwd)/creds.json

# Create service account
gcloud iam service-accounts create $SERVICE_ACCOUNT_NAME \
    --display-name="Terraform Service Account"

# Create and download service account key
gcloud iam service-accounts keys create $SA_CREDS_PATH \
    --iam-account=$SERVICE_ACCOUNT_NAME@$PROJECT_ID.iam.gserviceaccount.com
```

> NOTE: The Service Account credentials will be stored in your current directory under `creds.json`. **DO NOT COMMIT THIS FILE TO GIT**. The file name `creds.json` is included in the [`.gitignore`](.gitignore) of this repo as a precaution.


### 5. Grant Service Account access
This service account will need certain permissions to create, modify and destroy infrastructure in your GCP project. It will also need explicit access to the Terraform state bucket [created in the previous step](#gcs-bucket). To do so, you must bind particular IAM roles to your service account:

```bash
# Allow SA access to GCS bucket
gcloud storage buckets add-iam-policy-binding gs://$TF_BUCKET_NAME \
    --member=serviceaccount:$SERVICE_ACCOUNT_NAME@$PROJECT_ID.iam.gserviceaccount.com \
    --role=roles/storage.objectCreator

# Allow SA editor access to GCP project
## Feel free to fine tune this SA's permissions,
## rather than using the broad `editor` role.
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member "serviceAccount:$SERVICE_ACCOUNT_NAME@$PROJECT_ID.iam.gserviceaccount.com" \
    --role "roles/owner"
```

Lastly, you will need to export the path of the Service Account credentials as a Google Cloud defined environment variable in order for `terraform` to use it.

```bash
export GOOGLE_APPLICATION_CREDENTIALS=$SA_CREDS_PATH
echo $GOOGLE_APPLICATION_CREDENTIALS
```

## C. Initialiaze Terraform

You will be using `terraform` to manage all GCP infrastructure. [Follow the `terraform` installation instructions](https://developer.hashicorp.com/terraform/downloads) according to your OS. 

For example, on macOS:
```bash
brew tap hashicorp/tap
brew install hashicorp/tap/terraform
```

### 1. Configure Terraform backend

Edit the [`tf/backend.tf`](tf/backend.tf) file with your bucket name.
```hcl
terraform {
  backend "gcs" {
    bucket = "{YOUR BUCKET NAME}"
    prefix = "live/"
  }
}
```

### 2. Export TF_VARs
There are several variables Terraform will need that **should not** be committed to your repo. We will configure these in GitLab later. For now, set them as environment variables locally:
```bash
export TF_VAR_project_id="{YOUR GCP PROJECT ID}"
export TF_VAR_billing_account_id="{YOUR GCP BILLING ID}"
export TF_VAR_notification_email_address="{YOUR EMAIL ADDRESS}"
```

### 3. Test Terraform locally

Let's start by running a `terraform plan` and `terraform apply` locally! Then we will move on to managing via [GitLab CI pipelines](https://docs.gitlab.com/ee/ci/).

Be sure to export the GCP SA credentials path to `GOOGLE_APPLICATIONS_CREDENTIALS` first, [as mentioned previously](#5-grant-service-account-access).

```bash
terraform -chdir=tf/ init
terraform -chdir=tf/ plan
```

If all looks good in the plan, let's move this into automation!

## D. Automate with GitLab CI

Rather than running `terraform` commands locally, you can automate the process as well as better manage changes via GitLab CI! The GitLab CI pipeline configuration is stored in [`.gitlab-ci.yml`](.gitlab-ci.yml).

**[Check out this GitLab guide to learn more](https://docs.gitlab.com/ee/user/infrastructure/iac/)**.

### 1. GitLab CI Variables

The environment variables you export when testing locally ([in the previous step](#2-export-tf_vars)) will also need to be accessible by GitLab. You can do this by [creating project CI Variables](https://docs.gitlab.com/ee/ci/variables/). The following environment variables need to be stored as GitLab CI variables:
* `GCP_PROJECT_ID`: ID of your GCP Project
* `GCP_SERVICE_ACCOUNT_CREDS`: Service Account credentials JSON downloaded in previous step. Should be stored as `File` type variable.
* `NOTIFICATION_EMAIL_ADDRESS`: Email address to send Billing notifications to.
* `GCP_BILLING_ACCOUNT_ID`: ID of your GCP Billing account.

![GitLab CI Variables](./img/gitlab-ci-vars.png){width=50%}

### 2. Commit changes

Now that your GitLab project has access to its required CI Variables, it's time to commit your changes!

> :warning: BE SURE NOT TO COMMIT YOUR SERVICE ACCOUNT CREDENTIALS FILE!!

You should see changes to the `tf/backend.tf` file, and possibly the `tf/variables.tf` file if you made changes there.

```bash
git add tf/
git commit -m "configure tf backend"
git push
```

### 3. Run `terraform apply` via GitLab CI

Once your changes are committed to your own forked project, navigate to that project > `CI/CD` > `Pipelines`.

![GitLab CI/CD Pipelines](./img/cicd-pipelines.png){width=15%}

You should see a pipeline kick off that looks like this:

![GitLab CI Pipeline](./img/pipeline.png){width=50%}

Review the proposed Terraform changes by clicking the `build` job. If all looks good, run the `deploy` job to create your infrastructure.

## E. Access GKE cluster

Now that you've successfully run `terraform apply` via the `deploy` GitLab CI job, your GKE cluster should be available for use.

To pull your GKE `kubeconfig`, run the following:

```bash
CLUSTER_NAME=gke-$PROJECT_ID
gcloud components install gke-gcloud-auth-plugin
gcloud container clusters get-credentials $CLUSTER_NAME --region us-east1 --project $PROJECT_ID
```

Your cluster should now be your active `kubeconfig`. Check by running some `kubectl` commands.
```bash
kubectl config current-context
kubectl get pods -A
```